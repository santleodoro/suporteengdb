package dsiparomensagem.util;

/**
 * Classe respons�vel por conter apenas os caminhos dos diret�rios e arquivos
 * que os testes ir�o precisar para serem executados.
 * 
 * @author Sandra Rodrigues
 *
 */
public class ArquivoDiretorioUtil {
	private static String pastaRelatorioComData = DateUtil.getDataHora();
	private static String dirUser = System.getProperty("user.dir");
	public static String dirResource = "\\src\\test\\resources\\";
	public static String driverFile = dirUser.concat(dirResource).concat("drivers\\chromedriver.exe");
	public static String dirRelatorios = dirUser.concat(dirResource).concat(pastaRelatorioComData)
			.concat("\\relatorios\\");
	public static String dirArquivoProperties = dirUser.concat(dirResource).concat("properties\\");
}
