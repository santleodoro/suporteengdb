package dsiparomensagem.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class DateUtil {

	public static String getDataHora() {
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		return new SimpleDateFormat("yyyyMMdd_HHmm").format(ts);
	}
}
