package dsiparomensagem.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Classe que vai retornar os dados do arquivo dados.propertie
 * 
 * @author Sandra Rodrigues
 *
 */
public class LeitorProperties extends ArquivoDiretorioUtil {

	private String usuario;
	private String senha;
	private String mensagem;
	Properties propsDados = null;
	Properties propsMensagem = null;

	private Properties getPropDados() {
		propsDados = new Properties();
		try {
			FileInputStream file = new FileInputStream(dirArquivoProperties + "dados.properties");
			propsDados.load(file);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return propsDados;
	}

	private Properties getPropMensagem() {
		propsMensagem = new Properties();
		try {
			FileInputStream file = new FileInputStream(dirArquivoProperties + "mensagem.properties");
			propsMensagem.load(file);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return propsMensagem;
	}

	public LeitorProperties() {
		if (propsDados == null) {
			propsDados = getPropDados();
		}
		if (propsMensagem == null) {
			propsMensagem = getPropMensagem();
		}
		usuario = propsDados.getProperty("prop.usuario");
		senha = propsDados.getProperty("prop.senha");
		mensagem = propsMensagem.getProperty("prop.mensagem");
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
}
