package dsiparomensagem.objects;

import org.openqa.selenium.By;

/**
 * Classe que cont�m os mapeamentos dos campos da tela
 * 
 * @author Sandra Rodrigues
 *
 */
public class LoginObjects extends PilarObjects {

	public static By campoUsuario = By.xpath("//label[text()='Usu�rio']//following::input");
	public static By campoSenha = By.xpath("//label[text()='Senha']//following::input");
	public static By botaoLogin = By.xpath("//td[text()='Login  ']");
}
