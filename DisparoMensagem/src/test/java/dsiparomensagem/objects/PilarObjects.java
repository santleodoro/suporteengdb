package dsiparomensagem.objects;

import org.openqa.selenium.By;

/**
 * Classe responsável por conter o mapeamento de elementos comuns as todas as
 * classes de teste.
 * 
 * @author Sandra Rodrigues
 *
 */
public class PilarObjects {

	public static By botaoAdministracao = By
			.xpath("//img[@src='http://homologacao.sivsistema.com/images/icone_administracao.png']");
}
