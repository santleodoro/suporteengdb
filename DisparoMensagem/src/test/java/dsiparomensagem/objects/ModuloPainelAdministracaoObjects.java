package dsiparomensagem.objects;

import org.openqa.selenium.By;

/**
 * Classe responsável por conter o mapeamento de elementos comuns as todas as
 * classes de teste.
 * 
 * @author Sandra Rodrigues
 *
 */
public class ModuloPainelAdministracaoObjects {

	public static By botaoExpandirPilar = By.xpath(
			"//td[text()='Pilar Administração']/preceding-sibling::*//img[@src='http://homologacao.sivsistema.com/ambiental/sc/skins/Vli/images/TreeGrid/opener_closed.gif']");
	public static By moduloPainelAdministracao = By
			.xpath("//table[@role='presentation']//td[text()='Painel de administração']");
	public static By campoMensagem = By.name("Mensagem");
	public static By botaoEnviar = By.xpath("//td[text()='Enviar  ']");
}
