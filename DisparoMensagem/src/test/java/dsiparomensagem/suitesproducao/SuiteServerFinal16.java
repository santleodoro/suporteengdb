package dsiparomensagem.suitesproducao;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import dsiparomensagem.tests.LoginTest;
import dsiparomensagem.tests.ModuloPainelAdministracaoTest;
import dsiparomensagem.tests.PilarTest;
import dsiparomensagem.util.DriverUtil;

//Responsável por gerenciar as classes de testes que serão rodadas
@RunWith(Suite.class)
// Chamada das classes de teste
@SuiteClasses({

		LoginTest.class,

		PilarTest.class,

		ModuloPainelAdministracaoTest.class,

})

public class SuiteServerFinal16 {
	// Site que iremos acessar
	private static String url = "10.71.0.16";

	// Método com essa anotação informa que deve ocorrer antes de tudo que for
	// responsábilidade dessa classe
	@BeforeClass
	public static void setUp() {
		DriverUtil.configuraDriver();
		// Abre a URL informada
		DriverUtil.abrirUrl(url);
	}

	// Método com essa anotação informa que deve ocorrer depois de tudo que for
	// responsábilidade dessa classe
	@AfterClass
	public static void tearDown() {
		// Responsável por fechar o navegador depois que os testes terminam
		DriverUtil.fecharNavegador();
	}
}
