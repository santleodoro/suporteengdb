package dsiparomensagem.suitesproducao;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({

		SuiteServerFinal14.class,

		SuiteServerFinal15.class,

		SuiteServerFinal16.class,

		SuiteServerFinal17.class,

		SuiteServerFinal18.class,

})

public class SuitePrincipalProducao {

}
