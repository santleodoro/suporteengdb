package dsiparomensagem.tests;

import org.junit.After;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dsiparomensagem.pages.PilarPageObject;
import dsiparomensagem.util.LeitorProperties;
import dsiparomensagem.util.ScreenshotUtil;

/**
 * Classe respons�vel por executar os tests nas telas da aplica��o.
 * 
 * Aqui os testes est�o sendo chamados com um @Test para cada intera��o, prefiro
 * dessa forma, pois se houver erro de elemnto n�o encontrado em algum campo,
 * ser� poss�vel saber qual campo est� com problema.
 * 
 * @author Sandra Rodrigues
 *
 */
// Anota��o para informar que os testes devem ser chamados pela ordem dos nomes
// dos m�todos
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PilarTest {

	// Arquivo que ser� respons�vel por trazer os dados que ser�o utilizados nos
	// testes
	LeitorProperties properties = new LeitorProperties();

	// Classe respons�vel por conter os testes da p�gina de login da aplica��o a ser
	// testada
	PilarPageObject pilarPageObject = new PilarPageObject();

	private String metodoNome;

	// O m�todo after � chamado ap�s cada m�todo de @Test
	@After
	public void capiturarTelaDepoisDoTeste() {
		// Com essa chamada capturamos a tela que estamos testando
		// metodonome � o nome do arquivo que estamos criando, que � atualizado a cada
		// chamada dos m�todos de testes.
		ScreenshotUtil.printar("Pilar\\", metodoNome);
	}

	// Para que a suite entenda que o m�todo deve ser chamado ele tem que ser
	// anotado com @Test assim como JUnit, sem essa anota��o esse m�todo n�o ser�
	// chamado.
	@Test
	public void A001AcessarPilarAdministracao() {
		metodoNome = Thread.currentThread().getStackTrace()[1].getMethodName();
		pilarPageObject.clicarPilarAdministracao();
	}

}
