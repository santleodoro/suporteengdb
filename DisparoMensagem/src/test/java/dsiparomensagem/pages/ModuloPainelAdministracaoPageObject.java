package dsiparomensagem.pages;

import dsiparomensagem.objects.ModuloPainelAdministracaoObjects;
import dsiparomensagem.util.LeitorProperties;

/**
 * Classe respons�vel por reproduzir os passos do usu�rio e ou testador na tela.
 * 
 * @author Sandra Rodrigues
 *
 */
public class ModuloPainelAdministracaoPageObject {

	// A BasePage possui m�todos comuns de manipula��o das telas, como clique,
	// inser��o de textos, esperas, etc, pode ser chamada de qualquer classe de
	// PageObjects.
	BasePageObjects basePage = new BasePageObjects();

	// Arquivo que ser� respons�vel por trazer os dados que ser�o utilizados nos
	// testes
	LeitorProperties properties = new LeitorProperties();

	public ModuloPainelAdministracaoPageObject() {
	}

	public ModuloPainelAdministracaoPageObject expandirPainel() {
		basePage.clicar(ModuloPainelAdministracaoObjects.botaoExpandirPilar);
		return this;
	}

	public ModuloPainelAdministracaoPageObject clicarPainelAdministracao() {
		basePage.clicar(ModuloPainelAdministracaoObjects.moduloPainelAdministracao);
		return this;
	}

	public ModuloPainelAdministracaoPageObject informarMensagem(String textoMensagem) {
		basePage.informarTexto(ModuloPainelAdministracaoObjects.campoMensagem, textoMensagem);
		return this;
	}

	public ModuloPainelAdministracaoPageObject enviarMensagem() {
		basePage.clicar(ModuloPainelAdministracaoObjects.botaoEnviar);
		return this;
	}
}
