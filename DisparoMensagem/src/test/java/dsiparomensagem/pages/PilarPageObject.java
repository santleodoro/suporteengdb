package dsiparomensagem.pages;

import dsiparomensagem.objects.PilarObjects;
import dsiparomensagem.util.LeitorProperties;

/**
 * Classe respons�vel por reproduzir os passos do usu�rio e ou testador na tela.
 * 
 * @author Sandra Rodrigues
 *
 */
public class PilarPageObject {

	// A BasePage possui m�todos comuns de manipula��o das telas, como clique,
	// inser��o de textos, esperas, etc, pode ser chamada de qualquer classe de
	// PageObjects.
	BasePageObjects basePage = new BasePageObjects();

	// Arquivo que ser� respons�vel por trazer os dados que ser�o utilizados nos
	// testes
	LeitorProperties properties = new LeitorProperties();

	public PilarPageObject() {
	}

	public PilarPageObject clicarPilarAdministracao() {
		basePage.clicar(PilarObjects.botaoAdministracao);
		return this;
	}
}
