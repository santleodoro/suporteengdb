package dsiparomensagem.pages;

import dsiparomensagem.objects.LoginObjects;
import dsiparomensagem.util.LeitorProperties;

/**
 * Classe respons�vel por reproduzir os passos do usu�rio e ou testador na tela.
 * 
 * @author Sandra Rodrigues
 *
 */
public class LoginPageObject {

	// A BasePage possui m�todos comuns de manipula��o das telas, como clique,
	// inser��o de textos, esperas, etc, pode ser chamada de qualquer classe de
	// PageObjects.
	BasePageObjects basePage = new BasePageObjects();

	// Arquivo que ser� respons�vel por trazer os dados que ser�o utilizados nos
	// testes
	LeitorProperties properties = new LeitorProperties();

	public LoginPageObject() {
	}

	// A cada passo retornamos a pageobject para que ela consiga chamar o pr�ximo
	// passo com mais facilidade
	public LoginPageObject informarUsuario(String usuario) {
		basePage.informarTexto(LoginObjects.campoUsuario, usuario);
		return this;
	}

	public LoginPageObject informarSenha(String senha) {
		basePage.informarTexto(LoginObjects.campoSenha, senha);
		return this;
	}

	public LoginPageObject clicarBotaoLogin() {
		basePage.clicar(LoginObjects.botaoLogin);
		return this;
	}
}
